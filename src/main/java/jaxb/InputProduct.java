package jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.StringJoiner;

@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
public class InputProduct {

    private String description;
    private String gtin;
    private Price price;
    private String supplier;

    public String getDescription() {
        return description;
    }

    public String getGtin() {
        return gtin;
    }

    public Price getPrice() {
        return price;
    }

    public String getSupplier() {
        return supplier;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("description='" + description + "'")
                .add("gtin='" + gtin + "'")
                .add("price=" + price)
                .add("supplier='" + supplier + "'")
                .toString();
    }
}
