package jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
    https://howtodoinjava.com/jaxb/jaxb-exmaple-marshalling-and-unmarshalling-list-or-set-of-objects/
    https://www.journaldev.com/1234/jaxb-example-tutorial
*/

public class Main {

    public static void main(String[] args) throws JAXBException {
        OrderInput orderInput = readXml("src/main/resources/OrderInput.xml");
        System.out.println();
        Map<String, List<OutputProduct>> aggregateData = aggregateData(orderInput);
        System.out.println();
        writeXMLs(aggregateData);
    }

    private static OrderInput readXml(String inputPath) throws JAXBException {
        System.out.println("Reading input XML file...\n");
        JAXBContext jaxbContext = JAXBContext.newInstance(OrderInput.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        OrderInput orders = (OrderInput) jaxbUnmarshaller.unmarshal(new File(inputPath));

        for (Order order : orders.getOrderList()) {
            System.out.println(order);
        }

        return orders;
    }

    private static Map<String, List<OutputProduct>> aggregateData(OrderInput orderInput) {

        Map<String, List<OutputProduct>> aggregatedData = new HashMap<>();

        for (Order order : orderInput.getOrderList()) {
            for (InputProduct inputProduct : order.getProducts()) {
                if (aggregatedData.containsKey(inputProduct.getSupplier())) {
                    List<OutputProduct> outputProducts = aggregatedData.get(inputProduct.getSupplier());
                    outputProducts.add(new OutputProduct(
                            inputProduct.getDescription(),
                            inputProduct.getGtin(),
                            inputProduct.getPrice(),
                            order.getId()));
                } else {
                    List<OutputProduct> outputProducts = new ArrayList<>();
                    outputProducts.add(new OutputProduct(
                            inputProduct.getDescription(),
                            inputProduct.getGtin(),
                            inputProduct.getPrice(),
                            order.getId()));
                    aggregatedData.put(inputProduct.getSupplier(), outputProducts);
                }
            }
        }

        System.out.println("\nFinished aggregating data...\n");

        for (String supplier : aggregatedData.keySet()) {
            System.out.println("Products for supplier " + supplier);
            for (OutputProduct outputProduct : aggregatedData.get(supplier)) {
                System.out.println("\t" + outputProduct);
            }
        }

        return aggregatedData;
    }

    private static void writeXMLs(Map<String, List<OutputProduct>> aggregateData) throws JAXBException {
        for (String supplier : aggregateData.keySet()) {
            SupplierOutput supplierOutput = new SupplierOutput(supplier, aggregateData.get(supplier));
            JAXBContext jaxbContext = JAXBContext.newInstance(SupplierOutput.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            System.out.println("\nGenerating XML output...\n");
            marshaller.marshal(supplierOutput, System.out);

            marshaller.marshal(supplierOutput, new File("src/main/resources/" + supplierOutput.getSupplierName() + ".xml"));
        }
    }
}
