package jaxb;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {

    @XmlAttribute private String created;
    @XmlAttribute(name = "ID") private long id;

    public long getId() {
        return id;
    }

    @XmlElement(name = "product")
    private List<InputProduct> products;

    public List<InputProduct> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName()).append("[\n");
        sb.append("\tcreated: ").append(created).append('\n');
        sb.append("\tid: ").append(id).append('\n');
        sb.append("\tproducts: [").append('\n');
        for (InputProduct product : products) {
            sb.append("\t\t").append(product).append('\n');
        }
        sb.append("\t]").append('\n');
        sb.append("]");
        return sb.toString();
    }
}
