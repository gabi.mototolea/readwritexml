package jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.StringJoiner;

@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
public class OutputProduct {

    private String description;
    private String gtin;
    private Price price;
    private long orderid;

    public OutputProduct() {
    }

    public OutputProduct(String description, String gtin, Price price, long orderid) {
        this.description = description;
        this.gtin = gtin;
        this.price = price;
        this.orderid = orderid;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OutputProduct.class.getSimpleName() + "[", "]")
                .add("description='" + description + "'")
                .add("gtin='" + gtin + "'")
                .add("price=" + price)
                .add("orderid=" + orderid)
                .toString();
    }
}
