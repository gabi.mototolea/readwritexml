package jaxb;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.StringJoiner;

@XmlRootElement(name = "price")
@XmlAccessorType(XmlAccessType.FIELD)
public class Price {

    @XmlAttribute private String currency;
    @XmlValue private BigDecimal amount;

    @Override
    public String toString() {
        return new StringJoiner(", ", Price.class.getSimpleName() + "[", "]")
                .add("currency='" + currency + "'")
                .add("amount=" + amount)
                .toString();
    }
}
