package jaxb;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
public class SupplierOutput {

    @XmlTransient
    private String supplierName;

    @XmlElement(name = "product")
    private List<OutputProduct> productList;

    public String getSupplierName() {
        return supplierName;
    }

    public SupplierOutput() {
    }

    public SupplierOutput(String supplierName, List<OutputProduct> productList) {
        this.supplierName = supplierName;
        this.productList = productList;
    }
}
